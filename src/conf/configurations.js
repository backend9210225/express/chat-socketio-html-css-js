import dotenv from 'dotenv';
dotenv.config();

export const config = {
    port: process.env.PORT ?? 3000,
    dbUri: process.env.API_MONGO_URI ?? 'mongodb://localhost:27017/chat',
}