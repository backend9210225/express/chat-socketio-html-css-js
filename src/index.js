import {app} from "./app.js";
import {config} from './conf/configurations.js'
import {Server} from 'socket.io'
import {createServer} from 'node:http'
import {connect} from './db/connector.js'
const port = config.port

const httpServer = createServer(app)

const io = new Server(httpServer, {
    /**
     * Permite recuperar usuarios desconectados,
     * es decir, si un usuario se desconectó y el mensaje que se le envió no fue entregado,
     * será borrado posteriormente. Cuando se conecte nuevamente, no podrá ver el mensaje.
     * Para evitar esto, se puede usar el siguiente código:
     * */
    connectionStateRecovery:{
        maxDisconnectionDuration: 3000,
    }
})



const db = await connect()

io.on('connection',  async (socket) =>{
    console.log('a user connected')
    socket.on('chat message', async (msg) =>{
        console.log('message: ' + msg)
        await db.createMessage(msg)

        const lastInsertedMessage = await db.getFinalMessage()
        console.log(lastInsertedMessage[0]._id.toString())
        io.emit('chat message', msg, lastInsertedMessage[0]._id.toString())
    })
    socket.on('disconnect', () =>{
        console.log('user disconnected')
    })
    console.log('auth', socket.handshake.auth)

    if (!socket.recovered){
        const messages = await db.getMessages()
        messages.forEach(message =>{
            socket.emit('chat message', message.message, message._id.toString())
        })

    }
})
httpServer.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
})