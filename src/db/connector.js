import {MongoClient, ServerApiVersion} from 'mongodb'
import {config} from '../conf/configurations.js'

const uri = config.dbUri

const client = new MongoClient(uri, {
    serverApi:{
        version: ServerApiVersion.v1,
        strict: true
    }
})

export const connect = async () =>{
    try {
        await client.connect()
        await client.db('admin').command({ping: 1})
        console.log('Connected successfully to server')
        const createMessage = (msg) =>{
            return client.db('chat-cocket-io').collection('messages').insertOne({'message': msg})
        }
        const getMessages = async () =>{
            return await client.db('chat-cocket-io').collection('messages').find({}).toArray()
        }
        const getFinalMessage = async ()=>{
            return await client.db('chat-cocket-io').collection('messages').find({}).sort({_id:-1}).limit(1).toArray()
        }
        return {createMessage, getMessages, getFinalMessage}
    }catch (e) {
        await client.close()
        console.error(e)
    }
}