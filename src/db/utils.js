import {connect} from './connector.js'

const db = connect()
export const createMessage = (msg) =>{
    const result = db.then(client => {
        const collection = client.db('chat-cocket-io').collection('messages')
        collection.insertOne({'message': msg})
    })
}