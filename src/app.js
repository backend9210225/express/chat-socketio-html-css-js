import express from 'express';
import logger from 'morgan';
import {connect} from './db/connector.js'
const app = express();
app.use(logger('dev'));

app.get('/', (req, res) => {
  res.sendFile(process.cwd()+"/client/index.html")
});

export {app};